using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using WebAPI.DTO;
using Xunit;

namespace WebAPI.IntegrationTests
{
    public class IntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private HttpClient _client;
        public IntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public async System.Threading.Tasks.Task CreateProject()
        {
            var project = new Project()
            {
                Id = 12345,
                Name = "Viber",
                Description = "sth to do",
                AuthorId = 8,
                TeamId = 7,
                CreatedAt = DateTime.Now,
                Deadline = DateTime.Now
            };
            string jsonInString = JsonConvert.SerializeObject(project);
            var httpResponse = await _client.PostAsync("api/project/create",
                new StringContent(jsonInString, Encoding.UTF8, "application/json"));

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var createdProject = JsonConvert.DeserializeObject<Project>(stringResponse);

            await _client.DeleteAsync("api/project/delete/12345");

            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);
            Assert.Equal(project.Id, createdProject.Id);
            Assert.Equal(project.Name, createdProject.Name);
        }

        [Fact]
        public async System.Threading.Tasks.Task DeleteUser()
        {
            var user = new User()
            {
                Id = 12345,
                TeamId = 7,
                FirstName = "Elon",
                LastName = "Musk"
            };
            string jsonInString = JsonConvert.SerializeObject(user);
            await _client.PostAsync("api/user/create",
                  new StringContent(jsonInString, Encoding.UTF8, "application/json"));
            var httpResponse = await _client.DeleteAsync("api/user/delete/12345");

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var id = JsonConvert.DeserializeObject<decimal>(stringResponse);            

            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.Equal(user.Id, id);
        }

        [Fact]
        public async System.Threading.Tasks.Task DeleteUser_WhenNotExist()
        {
            var httpResponse = await _client.DeleteAsync("api/user/delete/12345");

            Assert.Equal(HttpStatusCode.BadRequest, httpResponse.StatusCode);
        }

        [Fact]
        public async System.Threading.Tasks.Task CreateTeam()
        {
            var team = new Team()
            {
                Id = 12345,
                Name = "Fiksiki"
            };
            string jsonInString = JsonConvert.SerializeObject(team);
            var httpResponse = await _client.PostAsync("api/team/create",
                new StringContent(jsonInString, Encoding.UTF8, "application/json"));

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var createdProject = JsonConvert.DeserializeObject<Team>(stringResponse);

            await _client.DeleteAsync("api/team/delete/12345");

            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);
            Assert.Equal(team.Id, createdProject.Id);
            Assert.Equal(team.Name, createdProject.Name);
        }

        [Fact]
        public async System.Threading.Tasks.Task DeleteTask()
        {
            var task = new Task()
            {
                Id = 12345,
                Name = "Nothing to do"
            };
            string jsonInString = JsonConvert.SerializeObject(task);
            await _client.PostAsync("api/task/create",
                  new StringContent(jsonInString, Encoding.UTF8, "application/json"));
            var httpResponse = await _client.DeleteAsync("api/task/delete/12345");

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var id = JsonConvert.DeserializeObject<decimal>(stringResponse);

            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.Equal(task.Id, id);
        }

        [Fact]
        public async System.Threading.Tasks.Task DeleteTask_WhenNotExist()
        {
            var httpResponse = await _client.DeleteAsync("api/task/delete/12345");

            Assert.Equal(HttpStatusCode.BadRequest, httpResponse.StatusCode);
        }

    }



    public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<Startup>
    {

    }
}

﻿using System.Collections.Generic;
using System.Linq;
using WebAPI.DTO;

namespace WebAPI.DAL
{
    public class TeamRepository : IRepository<Team>
    {
        private List<Team> _teams;

        public TeamRepository(IDataGeter<Team> dataGeter)
        {
            _teams = dataGeter.Get().ToList();
        }

        public void Create(Team item)
        {
            _teams.Add(item);
        }

        public void Delete(int id)
        {
            var item = _teams.Single(i => i.Id == id);
            _teams.Remove(item);
        }

        public Team Get(int id)
        {
            return GetAll().FirstOrDefault(u => u.Id == id);
        }

        public IEnumerable<Team> GetAll()
        {
            return _teams;
        }

        public void Update(Team item)
        {
            Delete(item.Id);
            _teams.Add(item);
        }
    }
}

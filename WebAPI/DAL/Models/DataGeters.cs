﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using WebAPI.DTO;

namespace WebAPI.DAL
{
    public class UsersDataGeter : IDataGeter<User>
    {
        public IEnumerable<User> Get()
        {
            return JsonConvert.DeserializeObject<IEnumerable<User>>
                (File.ReadAllText("Data/Users.json"));
        }
    }
    public class TasksDataGeter : IDataGeter<Task>
    {
        public IEnumerable<Task> Get()
        {
            return JsonConvert.DeserializeObject<IEnumerable<Task>>
                (File.ReadAllText("Data/Tasks.json"));
        }
    }

    public class TeamsDataGeter : IDataGeter<Team>
    {
        public IEnumerable<Team> Get()
        {
            return JsonConvert.DeserializeObject<IEnumerable<Team>>
                (File.ReadAllText("Data/Teams.json"));
        }
    }

    public class ProjctsDataGeter : IDataGeter<Project>
    {
        public IEnumerable<Project> Get()
        {
            return JsonConvert.DeserializeObject<IEnumerable<Project>>
                (File.ReadAllText("Data/Projects.json"));
        }
    }
}

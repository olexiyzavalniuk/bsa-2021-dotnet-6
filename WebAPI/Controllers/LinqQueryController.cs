﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.BLL;
using WebAPI.DTO;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LinqQueryController : Controller
    {
        private readonly Query _query = new Query();


        [Route("First/{id}")]
        [HttpGet]
        public async Task< ActionResult<Dictionary<Hierarchy, int>>> One(int id)
        {
            return Ok(await First.Run(id, _query));
        }

        [Route("Second/{id}")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Task2>>> Two(int id)
        {
            return Ok(await Second.Run(id, _query));
        }

        [Route("Third/{id}")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DTO.Third>>> ThreeAsync(int id)
        {
            return Ok(await BLL.Third.Run(id, _query));
        }
    }
}

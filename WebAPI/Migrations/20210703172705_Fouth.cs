﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebAPI.Migrations
{
    public partial class Fouth : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ProjectId",
                table: "Tasks",
                newName: "Project");

            migrationBuilder.RenameColumn(
                name: "PerformerId",
                table: "Tasks",
                newName: "Performer");

            migrationBuilder.RenameColumn(
                name: "TeamId",
                table: "Projects",
                newName: "Team");

            migrationBuilder.RenameColumn(
                name: "AuthorId",
                table: "Projects",
                newName: "Author");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Project",
                table: "Tasks",
                newName: "ProjectId");

            migrationBuilder.RenameColumn(
                name: "Performer",
                table: "Tasks",
                newName: "PerformerId");

            migrationBuilder.RenameColumn(
                name: "Team",
                table: "Projects",
                newName: "TeamId");

            migrationBuilder.RenameColumn(
                name: "Author",
                table: "Projects",
                newName: "AuthorId");
        }
    }
}

﻿using System.Collections.Generic;
using WebAPI.DTO;
using WebAPI.DAL;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.BLL
{
    public static class Third
    {
        public static async  Task<IEnumerable<DTO.Third>> Run(int id, Query query)
        {

            return await System.Threading.Tasks.Task.Run(() => query.Hierarchies
            .SelectMany(h => h.Tasks.Where(t => t.Performer.Id == id && t.FinishedAt != null
               && t.FinishedAt.Value.Year == 2021)).Select(t => new DTO.Third()
               {
                   id = t.Id,
                   name = t.Name,
               }));
        }
    }
}

﻿using System.Collections.Generic;
using WebAPI.DTO;
using WebAPI.DAL;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.BLL
{
    public static class First
    {
        public static async Task<Dictionary<Hierarchy, int>> Run(int id, Query query)
        {
            //Dictionary<Hierarchy, int> result;

            //result = query.Hierarchies.Where(hierarchy => hierarchy.Tasks.Any(task => task.Performer.Id == id)).
            //    ToDictionary(
            //    h => h,
            //    h => h.Tasks.Where(task => task.Performer.Id == id).Count()
            //    );


            return await System.Threading.Tasks.Task.Run(() => query.Hierarchies.Where(hierarchy =>
                    hierarchy.Tasks.Any(task => task.Performer.Id == id))
                    .ToDictionary(
                        h => h,
                        h => h.Tasks.Where(task => task.Performer.Id == id).Count()
                     ));
        }
    }
}

﻿using System.Collections.Generic;
using WebAPI.DTO;
using WebAPI.DAL;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.BLL
{
    public static class Second
    {
        public static async Task<IEnumerable<Task2>> Run(int id, Query query)
        {
            return await System.Threading.Tasks.Task.Run(() => 
                query.Hierarchies.Where(hierarchy => hierarchy.Tasks.Any(task => 
                task.Performer.Id == id && task.Name.Length < 45))
                .SelectMany(h => h.Tasks.Where(t => t.Performer.Id == id)));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UI.DTO
{
    public enum TaskState
    {
        ToDo = 1,
        InProgress,
        Done,
        Canceled
    }
}

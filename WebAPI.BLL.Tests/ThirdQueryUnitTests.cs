﻿using Xunit;
using WebAPI.DTO;
using WebAPI.BLL.Tests.Plugs;
using System.Collections.Generic;
using System;

namespace WebAPI.BLL.Tests
{
    public class ThirdQueryUnitTests
    {
        [Fact]
        public void RunThirdQuery_When2TasksIsNotFinished()
        {
            //Arrange
            var query = new QueryPlug();
            Hierarchy item = new Hierarchy();
            item.Tasks = new List<Task2>();
            User user = new User() { LastName = "Musk", FirstName = "Elon", Id = 8 };
            item.Tasks.Add(new Task2() { Name = "Task1", Performer = user });
            item.Tasks.Add(new Task2() { Name = "Task2", Performer = user });
            query.AddItem(item);

            //ACT
            var result = Third.Run(8, query);

            //ASSERT
            Assert.Empty(result);
        }

        [Fact]
        public void RunThirdQuery_When2TaskAndFinishedOnlyOne()
        {
            //Arrange
            var query = new QueryPlug();
            Hierarchy item = new Hierarchy();
            item.Tasks = new List<Task2>();
            User user = new User() { LastName = "Musk", FirstName = "Elon", Id = 8 };
            item.Tasks.Add(new Task2() { Name = "Task1", Performer = user });
            item.Tasks.Add(new Task2() { Name = "Task2", Performer = user, FinishedAt = DateTime.Now });
            item.Tasks[1].FinishedAt.Value.AddYears(2021);
            query.AddItem(item);

            //ACT
            var result = Third.Run(8, query);

            //ASSERT
            Assert.Single(result);
            Assert.Equal("Task2", result[0].name);
        }

        [Fact]
        public void RunThirdQuery_WhenListIsEmpty()
        {
            //Arrange
            var query = new QueryPlug();
            Hierarchy item = new Hierarchy();
            item.Tasks = new List<Task2>();

            //ACT
            var result = Third.Run(8, query);

            //ASSERT
            Assert.Empty(result);
        }
    }
}

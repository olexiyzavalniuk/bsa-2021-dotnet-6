﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.DAL;
using WebAPI.DTO;

namespace WebAPI.BLL.Tests.Plugs
{
    class UserDataGeterPlug : IDataGeter<User>
    {
        public IEnumerable<User> Get()
        {
            return new List<User>();
        }
    }

    class TaskDataGeterPlug : IDataGeter<DTO.Task>
    {
        public IEnumerable<DTO.Task> Get()
        {
            return new List<DTO.Task>();
        }
    }

}